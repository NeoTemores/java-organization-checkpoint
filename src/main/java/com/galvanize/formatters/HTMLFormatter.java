package com.galvanize.formatters;

import com.galvanize.Booking;

public class HTMLFormatter implements Formatter {
    @Override
    public String format(Booking booking) {
        StringBuilder result = new StringBuilder();

        result.append("<dl>\n");
        result.append(String.format("  <dt>Type</dt><dd>%s</dd>\n", String.valueOf(booking.getRoomType()).replace("_", " ")));
        result.append(String.format("  <dt>Room Number</dt><dd>%s</dd>\n", booking.getRoomNumber()));
        result.append(String.format("  <dt>Start Time</dt><dd>%s</dd>\n", booking.getStartTime()));
        result.append(String.format("  <dt>End Time</dt><dd>%s</dd>\n", booking.getEndTime()));
        result.append("</dl>");
        return result.toString();
    }
}
//String.valueOf(booking.getRoomType()).replace("_", " ")