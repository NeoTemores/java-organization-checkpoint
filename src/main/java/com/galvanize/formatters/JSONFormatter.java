package com.galvanize.formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter {
    @Override
    public String format(Booking booking) {
        StringBuilder result = new StringBuilder();

        result.append("{\n");
        result.append(String.format("  \"type\": \"%s\",\n", String.valueOf(booking.getRoomType()).replace("_", " ")));
        result.append(String.format("  \"roomNumber\": %s,\n", booking.getRoomNumber()));
        result.append(String.format("  \"startTime\": \"%s\",\n", booking.getStartTime()));
        result.append(String.format("  \"endTime\": \"%s\"\n", booking.getEndTime()));
        result.append("}");

        return result.toString();
    }
}

//String.valueOf(booking.getRoomType()).replace("_", " ")