package com.galvanize.formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter{
    @Override
    public String format(Booking booking){
        StringBuilder result = new StringBuilder();
        result.append("type,room number,start time,end time\n");
        result.append(String.format("%s,%s,%s,%s", String.valueOf(booking.getRoomType()).replace("_", " "), booking.getRoomNumber(), booking.getStartTime(), booking.getEndTime()));
        return result.toString();
    }
}
//String.valueOf(booking.getRoomType()).replace("_", " ")