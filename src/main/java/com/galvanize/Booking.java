package com.galvanize;

import java.util.List;

public class Booking {
    enum RoomType {
        Conference_Room,
        Suite,
        Auditorium,
        Classroom
    }

    private RoomType roomType;
    private String roomNumber;
    private String startTime;
    private String endTime;

    public Booking(RoomType roomType, String roomNumber, String startTime, String endTime) {
        this.roomType = roomType;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static Booking parse(String code) {
        //"r102-10:30am-11:00am"
        //return Array split parts ["r102", "10:30am", "11:00am"]
        String [] splitParts = code.split("-");


        List<String> codeParts = List.of(code.split("-"));
        char firstLetter = codeParts.get(0).charAt(0);
        //break up each list item
        String roomNumber = codeParts.get(0).substring(1);
        String startTime = codeParts.get(1);
        String endTime = codeParts.get(2);

        RoomType roomType = RoomType.Classroom;
        switch (firstLetter){
            case 'r':
                roomType= (RoomType.Conference_Room);
                break;
            case 's':
                roomType = (RoomType.Suite);
                break;
            case 'a':
                roomType = (RoomType.Auditorium);
                break;
        }

        return new Booking(roomType, roomNumber, startTime, endTime);
    }

    public RoomType getRoomType() {
        return this.roomType;
    }

    public String getRoomNumber() {
        return this.roomNumber;
    }

    public String getStartTime() {
        return this.startTime;
    }

    public String getEndTime() {
        return this.endTime;
    }

}
