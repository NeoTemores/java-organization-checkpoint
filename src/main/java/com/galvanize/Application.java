package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;

public class Application {
    public static void main(String[] args) {

        //takes 1st arg and parses the string and returns new booking instance
        Booking booking = Booking.parse(args[0]);

        // takes 2nd arg and returns the correct formatter instance type
        Formatter formatter = getFormatter(args[1]);

        System.out.println(formatter.format(booking));
    }

    public static Formatter getFormatter(String format) {
        //returns instance of new formatter
//        switch (format) {
//            case "json":
//                return new JSONFormatter();
//            case "csv":
//                return new CSVFormatter();
//            default:
//                return new HTMLFormatter();
//        }

        if(format.equals("json")){
            return new JSONFormatter();
        } else if( format.equals("csv")){
            return new CSVFormatter();
        } else{
            return new HTMLFormatter();
        }
    }

}