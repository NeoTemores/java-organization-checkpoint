package com.galvanize;

import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JSONFormatterTest {
    @Test
    public void testClassJSONFormatterMethodFormat() {
        Booking booking = new Booking(Booking.RoomType.Auditorium, "111", "08:30am", "11:00am");
        JSONFormatter jsonFormatter = new JSONFormatter();
        String actual = jsonFormatter.format(booking);

        String expected = "{\n" +
                String.format("  \"type\": \"%s\",\n", Booking.RoomType.Auditorium) +
                String.format("  \"roomNumber\": %s,\n", "111") +
                String.format("  \"startTime\": \"%s\",\n", "08:30am") +
                String.format("  \"endTime\": \"%s\"\n", "11:00am") +
                "}";

        assertEquals(expected, actual);
    }
}
