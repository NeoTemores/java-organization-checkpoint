package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.print.Book;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    // outContent.toString() will give you what your code printed to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void testMainSuite() {
        Application app = new Application();

        StringBuilder result = new StringBuilder();

        result.append("{\n");
        result.append("  \"type\": \"Suite\",\n");
        result.append("  \"roomNumber\": 111,\n");
        result.append("  \"startTime\": \"08:30am\",\n");
        result.append("  \"endTime\": \"11:00am\"\n");
        result.append("}\n");

        String expected = result.toString();
        String[] params = {"s111-08:30am-11:00am", "json"};
        app.main(params);

        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testMainConferenceRoom() {
        Application app = new Application();

        StringBuilder result = new StringBuilder();

        result.append("{\n");
        result.append("  \"type\": \"Conference Room\",\n");
        result.append("  \"roomNumber\": 111,\n");
        result.append("  \"startTime\": \"08:30am\",\n");
        result.append("  \"endTime\": \"11:00am\"\n");
        result.append("}\n");

        String expected = result.toString();
        String[] params = {"r111-08:30am-11:00am", "json"};
        app.main(params);

        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testMainClassroom() {
        Application app = new Application();

        StringBuilder result = new StringBuilder();

        result.append("{\n");
        result.append("  \"type\": \"Classroom\",\n");
        result.append("  \"roomNumber\": 111,\n");
        result.append("  \"startTime\": \"08:30am\",\n");
        result.append("  \"endTime\": \"11:00am\"\n");
        result.append("}\n");

        String expected = result.toString();
        String[] params = {"c111-08:30am-11:00am", "json"};
        app.main(params);

        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testMainAuditorium() {
        Application app = new Application();

        StringBuilder result = new StringBuilder();

        result.append("{\n");
        result.append("  \"type\": \"Auditorium\",\n");
        result.append("  \"roomNumber\": 111,\n");
        result.append("  \"startTime\": \"08:30am\",\n");
        result.append("  \"endTime\": \"11:00am\"\n");
        result.append("}\n");

        String expected = result.toString();
        String[] params = {"a111-08:30am-11:00am", "json"};
        app.main(params);

        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testGetFormatterJSON() {
        Formatter expected = new JSONFormatter();
        Formatter actual = Application.getFormatter("json");

        assertEquals(expected.getClass(), actual.getClass());
    }

    @Test
    public void testGetFormatterCSV() {
        Formatter expected = new CSVFormatter();
        Formatter actual = Application.getFormatter("csv");

        assertEquals(expected.getClass(), actual.getClass());
    }

    @Test
    public void testGetFormatterHTML() {
        Formatter expected = new HTMLFormatter();
        Formatter actual = Application.getFormatter("html");

        assertEquals(expected.getClass(), actual.getClass());
    }
}