package com.galvanize;

import com.galvanize.formatters.HTMLFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HTMLFormatterTest {
    @Test
    public void testClassHTMLFormatterMethodFormat() {
        Booking booking = new Booking(Booking.RoomType.Auditorium, "111", "08:30am", "11:00am");
        HTMLFormatter htmlFormatter = new HTMLFormatter();
        String actual = htmlFormatter.format(booking);
        String expected = "<dl>\n  <dt>Type</dt><dd>Auditorium</dd>\n  <dt>Room Number</dt><dd>111</dd>\n  <dt>Start Time</dt><dd>08:30am</dd>\n  <dt>End Time</dt><dd>11:00am</dd>\n</dl>";

        assertEquals(expected, actual);
    }
}
