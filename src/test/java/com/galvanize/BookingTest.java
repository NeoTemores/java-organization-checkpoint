package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookingTest {
    @Test
    public void testBookingParse(){
//        Booking expected = new Booking(Booking.RoomType.Auditorium, "111", "08:30am", "11:00am");
        Booking actual = Booking.parse("a111-08:30am-11:00am");

        assertEquals(Booking.RoomType.Auditorium, actual.getRoomType());
        assertEquals("111", actual.getRoomNumber());
        assertEquals("08:30am", actual.getStartTime());
        assertEquals("11:00am", actual.getEndTime());
    }
}
