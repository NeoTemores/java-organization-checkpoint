package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CSVFormatterTest {
    @Test
    public void testClassCVSFormatterMethodFormat(){
        Booking booking = new Booking(Booking.RoomType.Auditorium, "111", "08:30am", "11:00am");
        CSVFormatter cvsFormatter = new CSVFormatter();
        String actual = cvsFormatter.format(booking);
        String expected = "type,room number,start time,end time\nAuditorium,111,08:30am,11:00am";

        assertEquals(expected, actual);
    }
}
